<?php

namespace AppBundle\Controller;

use Unirest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;

class QvoController extends Controller
{
    //Header para acceder a endpoints de QVO
    public $headers = array('Accept' => 'application/json','Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV9rVWpuTE9BRzN0ak5tdFVETHRfS2p3IiwiYXBpX3Rva2VuIjp0cnVlfQ.ZK3qN-7JinKqncgyDMP9U6ub1rSR87rUXmA5E84iQVw');

    /**
     * @Route("/qvo", name="Inicio")
     */
    public function indexAction(Request $request)
    {
        if ($request->getMethod() == Request::METHOD_POST){
            $name = $request->request->get('name');
            $email = $request->request->get('email');

            //Crear Cliente
            $query = array('email'=>$email,'name'=>$name);
            $createResponse = Unirest\Request::post('https://playground.qvo.cl/customers',$this->headers,$query);

            //Actualizar la lista
            $response = Unirest\Request::get('https://playground.qvo.cl/customers',$this->headers);
            return $this->render('qvo/index.html.twig',array(
              'customers' => $response->body
            ));
        }

        //Muestra la lista
        $response = Unirest\Request::get('https://playground.qvo.cl/customers',$this->headers);
        return $this->render('qvo/index.html.twig',array(
          'customers' => $response->body
        ));
        //dump($response->body);
    }
}
