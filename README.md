desafio_asech
=============

A Symfony project created on November 15, 2017, 7:16 pm.

Proyecto con funciones básicas para el uso de una api como la de QVO por medio del framework de Symfony ver. 3.x

+ Configuración Apache, para no tener problemas la app con las rutas en modo desarrollo.

Archivo: [ httpd-vhots.conf ]
   
    ```sh
    <VirtualHost *:80>
    	ServerName desafioAsech
    	DocumentRoot "${INSTALL_DIR}/www/desafio_asech/web/app_dev.php"
    	<Directory  "${INSTALL_DIR}/www/desafio_asech/web/">
    		Options +Indexes +Includes +FollowSymLinks +MultiViews
    		AllowOverride All
    		Require local
    	</Directory>
    </VirtualHost>
    ```
+ Se agregó [Composer](https://getcomposer.org/).

+ Se agregó [Unirest](http://unirest.io/) para las consultas a APIS.
